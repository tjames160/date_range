﻿using System;
using System.Globalization;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DateRangeTests
{
    [TestClass()]
    public class UnitTest1
    {
        [TestMethod()]
        public void MainTest()
        {
            var culture = CultureInfo.CreateSpecificCulture("pl-PL");
            Thread.CurrentThread.CurrentUICulture = culture;
            var format = culture.DateTimeFormat.ShortDatePattern;
            DateTime firstDate = Convert.ToDateTime("01/01/1970");
            DateTime secondDate = Convert.ToDateTime("01/01/1980");

            Assert.AreEqual(CreateDateRange(format, firstDate, secondDate));
        }
    }
}

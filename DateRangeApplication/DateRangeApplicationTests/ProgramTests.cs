﻿using System;
using System.Globalization;
using System.Threading;
using DateRangeApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DateRangeApplicationTests
{
    [TestClass]
    public class ProgramTests
    {
        [TestMethod]
        public void CreateDateRangeTest1()
        {
            var culture = CultureInfo.CreateSpecificCulture("pl-PL");
            Thread.CurrentThread.CurrentUICulture = culture;
            Thread.CurrentThread.CurrentCulture = culture;
            var format = culture.DateTimeFormat.ShortDatePattern;

            Assert.AreEqual("11.01.1970 - 16.01.1980",
                Program.CreateDateRange(format, Convert.ToDateTime("11.01.1970"), Convert.ToDateTime("16.01.1980")));

            Assert.AreEqual("11.01 - 16.06.1970",
                Program.CreateDateRange(format, Convert.ToDateTime("11.01.1970"), Convert.ToDateTime("16.06.1970")));

            Assert.AreEqual("11 - 16.01.1970",
                Program.CreateDateRange(format, Convert.ToDateTime("11.01.1970"), Convert.ToDateTime("16.01.1970")));

            Assert.AreEqual("16.01.1970",
                Program.CreateDateRange(format, Convert.ToDateTime("16.01.1970"), Convert.ToDateTime("16.01.1970")));
        }

        [TestMethod]
        public void CreateDateRangeTest2()
        {
            var culture = CultureInfo.CreateSpecificCulture("en-US");
            Thread.CurrentThread.CurrentUICulture = culture;
            Thread.CurrentThread.CurrentCulture = culture;
            var format = culture.DateTimeFormat.ShortDatePattern;

            Assert.AreEqual("1/16/1970 - 1/16/1980", 
                Program.CreateDateRange(format, Convert.ToDateTime("01.16.1970"), Convert.ToDateTime("01.16.1980")));

            Assert.AreEqual("1/11 - 6/16/1970", 
                Program.CreateDateRange(format, Convert.ToDateTime("01.11.1970"), Convert.ToDateTime("06.16.1970")));

            Assert.AreEqual("1/11 - 16/1970", 
                Program.CreateDateRange(format, Convert.ToDateTime("01.11.1970"), Convert.ToDateTime("01.16.1970")));

            Assert.AreEqual("1/16/1970", 
                Program.CreateDateRange(format, Convert.ToDateTime("01.16.1970"), Convert.ToDateTime("01.16.1970")));
        }

        [TestMethod]
        public void CreateDateRangeTest3()
        {
            var culture = CultureInfo.CreateSpecificCulture("ja-JP");
            Thread.CurrentThread.CurrentUICulture = culture;
            Thread.CurrentThread.CurrentCulture = culture;
            var format = culture.DateTimeFormat.ShortDatePattern;

            Assert.AreEqual("1970/01/16 - 1980/01/16",
                Program.CreateDateRange(format, Convert.ToDateTime("1970/01/16"), Convert.ToDateTime("1980/01/16")));

            Assert.AreEqual("1970/06/16 - 01/11",
                Program.CreateDateRange(format, Convert.ToDateTime("1970/01/11"), Convert.ToDateTime("1970/06/16")));

            Assert.AreEqual("1970/01/11 - 16",
                Program.CreateDateRange(format, Convert.ToDateTime("1970/01/11"), Convert.ToDateTime("1970/01/16")));

            Assert.AreEqual("1970/01/16",
                Program.CreateDateRange(format, Convert.ToDateTime("01.16.1970"), Convert.ToDateTime("01.16.1970")));
        }
    }
}
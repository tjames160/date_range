﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DateRangeApplication
{
    public class Program
    {
        public static void Main()
        {
            var culture = CultureInfo.CurrentCulture;

            var firstMessageLine1 = LocalisedText.firstMessageLine1;
            var firstMessageLine2 = LocalisedText.firstMessageLine2;
            var firstDateMessage = LocalisedText.firstDateMessage;
            var secondDateMessage = LocalisedText.secondDateMessage;
            var incorrectDateMessage = LocalisedText.incorrectDateMessage;
            var helpMessageLine1 = LocalisedText.helpMessageLine1;
            var helpMessageLine2 = LocalisedText.helpMessageLine2;
            var restartMessage = LocalisedText.restartMessage;
            var exitMessage = LocalisedText.exitMessage;
            var incorrectCommandMessage = LocalisedText.incorrectCommandMessage;
            var dateRangeMessage = LocalisedText.dateRangeMessage;

            var format = culture.DateTimeFormat.ShortDatePattern;

            Console.WriteLine("\n" + firstMessageLine1 +
                              "\n\n" + firstMessageLine2 +
                              "\n\n" + firstDateMessage + "\n");
            var strFirstDate = Console.ReadLine();
            while (strFirstDate == null)
            {
                Console.WriteLine("\n" + firstDateMessage + "\n");
                strFirstDate = Console.ReadLine();
            }
            CheckIfCommandOrHelpRequest(strFirstDate, helpMessageLine1, helpMessageLine2, restartMessage, exitMessage, incorrectCommandMessage);
            strFirstDate = strFirstDate.Trim();
            var firstDatePassed = DateTime.TryParse(strFirstDate, out var firstDate);
            while (firstDatePassed == false)
            {
                Console.WriteLine("\n" + incorrectDateMessage + "\n");
                strFirstDate = Console.ReadLine();
                CheckIfCommandOrHelpRequest(strFirstDate, helpMessageLine1, helpMessageLine2, restartMessage, exitMessage, incorrectCommandMessage);
                firstDatePassed = DateTime.TryParse(strFirstDate, out firstDate);
            }

            Console.WriteLine("\n" + secondDateMessage + "\n");
            var strSecondDate = Console.ReadLine();
            while (strSecondDate == null)
            {
                Console.WriteLine("\n" + secondDateMessage + "\n");
                strSecondDate = Console.ReadLine();
            }
            CheckIfCommandOrHelpRequest(strFirstDate, helpMessageLine1, helpMessageLine2, restartMessage, exitMessage, incorrectCommandMessage);
            strSecondDate = strSecondDate.Trim();
            var secondDatePassed = DateTime.TryParse(strSecondDate, out var secondDate);
            while (secondDatePassed == false)
            {
                Console.WriteLine("\n" + incorrectDateMessage + "\n");
                strSecondDate = Console.ReadLine();
                CheckIfCommandOrHelpRequest(strFirstDate, helpMessageLine1, helpMessageLine2, restartMessage, exitMessage, incorrectCommandMessage);
                secondDatePassed = DateTime.TryParse(strSecondDate, out secondDate);
            }

            var dateRange = CreateDateRange(format, firstDate, secondDate);

            Console.WriteLine("\n" + dateRangeMessage);
            Console.WriteLine("\n" + dateRange + "\n");
            
            var command = Console.ReadLine();
            RunCommand(command, incorrectCommandMessage);
        }
        
        public static string CreateDateRange(string format, DateTime firstDate, DateTime secondDate)
        {
            DateTime earliestDate;
            DateTime latestDate;
            string dateRange;

            if (firstDate < secondDate)
            {
                earliestDate = firstDate;
                latestDate = secondDate;
            }
            else if (firstDate > secondDate)
            {
                earliestDate = secondDate;
                latestDate = firstDate;
            }
            else
            {
                return firstDate.ToString(format);
            }

            var earliestYear = earliestDate.Year;
            var latestYear = latestDate.Year;
            if (earliestYear != latestYear)
            {
                dateRange = YearRange(format, earliestDate, latestDate);
            }
            else
            {
                var earliestMonth = earliestDate.Month;
                var latestMonth = latestDate.Month;

                dateRange = earliestMonth == latestMonth ? 
                    DayRange(format, earliestDate, latestDate) : 
                        MonthRange(format, earliestDate, latestDate);
            }
            return dateRange;
        }

        private static string DayRange(string format, DateTime earliestDate, DateTime latestDate)
        {
            string dateRange = String.Empty;

            var earliestDay = earliestDate.Day;
            var strEarliestDay = earliestDay.ToString();
            if (earliestDay < 10)
            {
                strEarliestDay = 0 + strEarliestDay;
                
            }

            var latestDay = latestDate.Day;
            var strLatestDay = latestDay.ToString();
            if (latestDay < 10)
            {
                strLatestDay = 0 + strLatestDay;
            }

            var month = earliestDate.Month;
            var strMonth = month.ToString();
            if (month < 10)
            {
                strMonth = 0 + strMonth;
            }

            var year = earliestDate.Year;

            switch (format)
            {
                case "dd/MM/yyyy":
                    dateRange = strEarliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "MM/dd/yyyy":
                    dateRange = strMonth + "/" + strEarliestDay + " " + "-" + " " + strLatestDay + "/" + year;
                    break;
                case "yyyy/MM/dd":
                    dateRange = year + "/" + strMonth + "/" + strEarliestDay + " " + "-" + " " + strLatestDay;
                    break;
                case "yyyy/dd/MM":
                    dateRange = year + "/" + strEarliestDay + " " + "-" + " " + strLatestDay + "/" + strMonth;
                    break;
                case "d/M/yyyy":
                    dateRange = earliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "M/d/yyyy":
                    dateRange = month + "/" + earliestDay + " " + "-" + " " + latestDay + "/" + year;
                    break;
                case "yyyy/M/d":
                    dateRange = year + "/" + month + "/" + earliestDay + " " + "-" + " " + latestDay;
                    break;
                case "yyyy/d/M":
                    dateRange = year + "/" + earliestDay + " " + "-" + " " + latestDay + "/" + month;
                    break;
                case "dd.MM.yyyy":
                    dateRange = strEarliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "MM.dd.yyyy":
                    dateRange = strMonth + "." + strEarliestDay + " " + "-" + " " + strLatestDay + "." + year;
                    break;
                case "yyyy.MM.dd":
                    dateRange = year + "." + strMonth + "." + strEarliestDay + " " + "-" + " " + strLatestDay;
                    break;
                case "yyyy.dd.MM":
                    dateRange = year + "." + strEarliestDay + " " + "-" + " " + strLatestDay + "." + strMonth;
                    break;
                case "d.M.yyyy":
                    dateRange = earliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "M.d.yyyy":
                    dateRange = month + "." + earliestDay + " " + "-" + " " + latestDay + "." + year;
                    break;
                case "yyyy.M.d":
                    dateRange = year + "." + month + "." + earliestDay + " " + "-" + " " + latestDay;
                    break;
                case "yyyy.d.M":
                    dateRange = year + "." + earliestDay + " " + "-" + " " + latestDay + "." + month;
                    break;
                case "dd-MM-yyyy":
                    dateRange = strEarliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "MM-dd-yyyy":
                    dateRange = strMonth + "-" + strEarliestDay + " " + "-" + " " + strLatestDay + "-" + year;
                    break;
                case "yyyy-MM-dd":
                    dateRange = year + "-" + strMonth + "-" + strEarliestDay + " " + "-" + " " + strLatestDay;
                    break;
                case "yyyy-dd-MM":
                    dateRange = year + "-" + strEarliestDay + " " + "-" + " " + strLatestDay + "-" + strMonth;
                    break;
                case "d-M-yyyy":
                    dateRange = earliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "M-d-yyyy":
                    dateRange = month + "-" + earliestDay + " " + "-" + " " + latestDay + "-" + year;
                    break;
                case "yyyy-M-d":
                    dateRange = year + "-" + month + "-" + earliestDay + " " + "-" + " " + latestDay;
                    break;
                case "yyyy-d-M":
                    dateRange = year + "-" + earliestDay + " " + "-" + " " + latestDay + "-" + month;
                    break;
            }

            return dateRange;
        }

        private static string MonthRange(string format, DateTime earliestDate, DateTime latestDate)
        {
            string dateRange = String.Empty;

            var earliestDay = earliestDate.Day;
            var strEarliestDay = earliestDay.ToString();
            if (earliestDay < 10)
            {
                strEarliestDay = 0 + strEarliestDay;
            }

            var earliestMonth = earliestDate.Month;
            var strEarliestMonth = earliestMonth.ToString();
            if (earliestMonth < 10)
            {
                strEarliestMonth = 0 + strEarliestMonth;
            }

            switch (format)
            {
                case "dd/MM/yyyy":
                    dateRange = strEarliestDay + "/" + strEarliestMonth + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "MM/dd/yyyy":
                    dateRange = strEarliestMonth + "/" + strEarliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "yyyy/MM/dd":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + strEarliestMonth + "/" + strEarliestDay;
                    break;
                case "yyyy/dd/MM":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + strEarliestDay + "/" + strEarliestMonth;
                    break;
                case "d/M/yyyy":
                    dateRange = earliestDay + "/" + earliestMonth + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "M/d/yyyy":
                    dateRange = earliestMonth + "/" + earliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "yyyy/M/d":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + earliestMonth + "/" + earliestDay;
                    break;
                case "yyyy/d/M":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + earliestDay + "/" + earliestMonth;
                    break;
                case "dd.MM.yyyy":
                    dateRange = strEarliestDay + "." + strEarliestMonth + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "MM.dd.yyyy":
                    dateRange = strEarliestMonth + "." + strEarliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "yyyy.MM.dd":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + strEarliestMonth + "." + strEarliestDay;
                    break;
                case "yyyy.dd.MM":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + strEarliestDay + "." + strEarliestMonth;
                    break;
                case "d.M.yyyy":
                    dateRange = earliestDay + "." + earliestMonth + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "M.d.yyyy":
                    dateRange = earliestMonth + "." + earliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "yyyy.M.d":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + earliestMonth + "." + earliestDay;
                    break;
                case "yyyy.d.M":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + earliestDay + "." + earliestMonth;
                    break;
                case "dd-MM-yyyy":
                    dateRange = strEarliestDay + "-" + strEarliestMonth + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "MM-dd-yyyy":
                    dateRange = strEarliestMonth + "-" + strEarliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "yyyy-MM-dd":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + strEarliestMonth + "-" + strEarliestDay;
                    break;
                case "yyyy-dd-MM":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + strEarliestDay + "-" + strEarliestMonth;
                    break;
                case "d-M-yyyy":
                    dateRange = earliestDay + "-" + earliestMonth + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "M-d-yyyy":
                    dateRange = earliestMonth + "-" + earliestDay + " " + "-" + " " + latestDate.ToString(format);
                    break;
                case "yyyy-M-d":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + earliestMonth + "-" + earliestDay;
                    break;
                case "yyyy-d-M":
                    dateRange = latestDate.ToString(format) + " " + "-" + " " + earliestDay + "-" + earliestMonth;
                    break;
            }

            return dateRange;
        }

        private static string YearRange(string format, DateTime earliestDate, DateTime latestDate)
        {
            string dateRange = earliestDate.ToString(format) + " " + "-" + " " + latestDate.ToString(format);

            return dateRange;
        }

        private static void DisplayHelpMessage(string helpMessageLine1, string helpMessageLine2, string restartMessage, 
                                                    string exitMessage, string incorrectCommandMessage)
        {
            Console.WriteLine("\n" + helpMessageLine1 +
                              "\n\n" + helpMessageLine2 +
                              "\n" + restartMessage  +
                              "\n" + exitMessage + "\n");
            var command = Console.ReadLine();

            RunCommand(command, incorrectCommandMessage);
        }

        private static void RunCommand(string command, string incorrectCommandMessage)
        {
            command = command.ToUpper();

            switch (command)
            {
                case "RESTART":
                    Main();
                    break;
                case "R":
                    Main();
                    break;
                case "EXIT":
                    Environment.Exit(0);
                    break;
                case "E":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("\n" + incorrectCommandMessage);
                    Main();
                    break;
            }
        }

        private static void CheckIfCommandOrHelpRequest(string strToCheck, string helpMessageLine1, string helpMessageLine2, 
                                                            string restartMessage, string exitMessage, string incorrectCommandMessage)
        {
            strToCheck = strToCheck.ToUpper();

            switch (strToCheck)
            {
                case "HELP":
                    DisplayHelpMessage(helpMessageLine1, helpMessageLine2, restartMessage, exitMessage, incorrectCommandMessage);
                    break;
                case "H":
                    DisplayHelpMessage(helpMessageLine1, helpMessageLine2, restartMessage, exitMessage, incorrectCommandMessage);
                    break;
                case "RESTART":
                    RunCommand(strToCheck, incorrectCommandMessage);
                    break;
                case "R":
                    RunCommand(strToCheck, incorrectCommandMessage);
                    break;
                case "EXIT":
                    RunCommand(strToCheck, incorrectCommandMessage);
                    break;
                case "E":
                    RunCommand(strToCheck, incorrectCommandMessage);
                    break;
            }
        }
    }
}
